\section{Introduction}

Oscillators are basic components of many communication, navigation, and measurement systems. They provide a periodic output that can be used for wireless transmission, high speed digital systems and clocking functions, etc. By slightly shifting the frequency of these oscillations with voltage controlled oscillators (VCO) we can communicate wirelessly. VCOs are used in consumer electronics products, operating on many different wireless communication standards. These devices however have low tunability, meaning that they can only transmit in narrow frequency intervals. Therefore a transmission device must have several of these VCO in order to be able to transmit signals in several communications standards (wireless, bluetooth, phone network, etc.). With a rapidly increasing number of different communication standards, the current situation with individual chips emmiting for each separate standard is becoming unfeasible due to CMOS scaling  power consumption, and multiple standard requirements \cite{intro}. Now more than ever, there is a strong interest, driven by cost and performance, to develop oscillators with a wide frequency band. 

\begin{figure}[H]
\centering
\resizebox{.6\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/wireless.jpg}
  \unskip
   }
\captionsetup{type=figure}
    \caption{Increasing number of comunication standards}
    \label{fig:wireless}
\end{figure}


Spin transfer nano oscillators (STNO) are devices based on electron spin properties that promise to have higher tunability, as well as a much lower size when comparing to regular VCOs. These spintronic oscillators can be used for applications such as microwave emission, frequency modulation, frequency mixing and frequency detection. However, some issues arise from using STNOs such as low output power, high spectral linewidth (phase noise), the need for an external magnetic field and of a high input current density. To this end, several efforts are presently underway to improve the performance of spin torque nano-oscillators. Also, there remains a great deal of variability in device properties, even within a single lab. This suggests that the performance of a nano-oscillator is highly afected by the variability of fabrication results at the nanoscale. To improve device repeatability and performance, much work is required to improve our basic understanding of spin torque nano-oscillator operation by systematically examining how the ‘‘real-world’’ details of the spin torque nano-oscillator device alter the essential physics. 


\section{Theoretical Background}
\subsection{Spin transfer nano oscillators}
It has been known since 1935 \cite{antigo} that the magnetization $\vec{M}$ of a ferromagnetic material has a precessional motion when under an external magnetic field $\vec{B}$. In other words, when an external magnetic field is applied its magnetization oscillates. For free oscillations under this conditions the Kittel formula gives us the resonation frequency of the precession.
\[ f=\frac{\gamma}{2\pi}\sqrt{B(B+\mu _{0}M)} \]
Where $\gamma$ is the gyromagnetic ratio, related with the ratio of the magnetic moment and angular moment of the system when $\vec{M}$ and $\vec{B}$ are not aligned.

However, due to loss of energy, this oscillation stops soon after the external field is applied, meaning that it couldn't be used as an oscillator. In common voltage controlled oscillators (VCO) a LC resonator with an amplifier is used to resupply the energy loss. This method can't be used to resupply magnetization precessional movement, therefore a new method of resuplly is required. One solution found for nano oscillators is based on spin momentum transfer torque. In order to understand this property some knowledge in quantum mechanics is needed.

\begin{figure}[H]
\centering
\resizebox{.45\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/torque.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{\textit{Spin momentum transfer}}
    \label{fig:stt}
\end{figure}

Electrons have a spin, a small quantity of angular momentum intrinsic to the electron characterized by the quantum number $1/2$ or $-1/2$, depending on whether they are \textit{spin-up} or \textit{spin-down} respectively. Usually, an electrical current is unpolarized (meaning roughly half of its eletrons are \textit{spin-up} and the other half are \textit{spin-down}), but it can be polarized by passing through a fixed magnetic layer (polarizing layer). When this current passes through a free magnetic layer it transfers its spin momentum to the layer creating a torque acting on the magnetic layer magnetization. If the current density is larger than a critical current density, the momentum transferred allows the compensation of the natural magnetic relaxation of the system and the generation of sustain magnetization oscillations. Since the total momentum transferred is related to the amplitude and frequency of the precession, we can control its frequency by tuning the current density (Current Controlled Oscillator). The spin momentum tranfer effect is only predominant for small sizes, therefore any device using this effect must have $100nm$ or less. The critical current density required for this effect is in the order of $J_c~10^{7}A/cm^2$, meaning a current $I_{dc}\approx 1mA$, for our device size. In Figure \ref{fig:stt} it is possible to observe the several states of the polarization of electrons of a current as it goes through the several layers described before.

\begin{figure}[H]
\centering
\resizebox{.35\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/mtj_i.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{MTJ schematic}
    \label{fig:mtj}
\end{figure}


Now that we have an oscillator we need to convert the oscillation of magnetization to an oscillation in voltage. Lets now assume that the layers mentioned above belong to a magnetic tunnel junction (MTJ) where they are separated by an insulator, as shown in Figure \ref{fig:mtj}. In a MTJ, through tunneling magnetic resistance (TMR) the relative orientation of the two magnetic layers (fixed and free) is related to its resistance by $\Delta R \propto cos(\alpha)$, where $\alpha$ is the angle between magnetic orientations. The magneto-resistance coefficient is determined by
\[MR=\frac{R_{ap}-R_p}{R_p}\]
where $R_p$ is the minimum resistance, when both magnetizations are aligned parallel, and $R_{ap}$ is the maximum resistance, when the magnetizations are aligned anti-parallel. In figure \ref{fig:mtj2} it is possible to observe all the described properties of a MTJ. By forcing a current trough the MTJ we can measure the voltage $V=I_{dc} \cdot R$.

\begin{figure}[H]
\centering
\resizebox{.35\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/magnetor.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{\textit{Magnetoresistance}}
    \label{fig:mtj2}
\end{figure}


The whole process of generation of voltage oscillations is shown in figure \ref{fig:process}.

\begin{figure}[H]
\centering
\resizebox{.7\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/process.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{Nano oscillator principle of functioning}
    \label{fig:process}
\end{figure}


A spin transfer nano-oscillator converts the oscillation of the magnetization of the free layer, created through spin momentum transfer, in a voltage oscillation, measured by the tunneling magnetic resistance. The resulting voltage will have a DC component $R_p < \frac{V_{dc}}{I_{dc}} < R_{ap}$ and an oscillating component $\Delta V$ with frequency determined by the original magnetization precession frequency created.

In order to use a nano-oscillator for radio frequency (RF), some requirements must be met. We need to obtain the same oscillations in a zero applied magnetic field, maximize the precession amplitude and the output power and minimize the phase noise, determined by its spectrum linewidth. To obtain the oscillations without the external field and to maximize the precession choosing a suitable magnetization orientation of the polarizing layer might be a solution. However, to reach acceptable levels of output power and linewidth further tuning of components is needed.

The maximum power output is given by the formula $P_{max}=V_{max}\cdot I_{max}$ which in the case of a nano oscillator translates to 

\[ P_{out}=\left(\frac{\Delta R}{R}\right)^2 \cdot \frac{\beta}{(\beta +1)^2} \cdot R \cdot I \]

where $\beta$ is the ratio between the resistance of the MTJ '$R$' and the total impedance of the system '$Z$'. By choosing different materials for the MTJ, for example, it is possible to obtain values of R and TMR coeficient that allow for a better power output. For a common MgO based MTJ with radius $r=100nm$ it is possible to obtain a theoretical power output between -40dBm and -30dBm, which is stil far from the required 0dB for a RF oscillator. Another factor we can observe in this formula is a linear dependence with the current '$I$', however there is also a need to use low current densities for industrial purposes, therefore current shouldn't be used to increase the output power.

Several efforts have been made to solve this problem. In the next paragraphs we will describe some of those experiments and discuss its results.

\subsection{State of the art}

Zeng (et al.)(2013)\cite{ultra} have experimented a MTJ with a core magnetic stack consisting of a synthetic antiferromagnet (SAF) $Co_{70}Fe_{30}/Ru/Co_{40}Fe_{40}B_{20}$ layer and a $Co_{20}Fe_{60}B_{20}$ free layer (FL)
separated by a MgO insulator as shown in figure \ref{fig:ultraa} . By using this MTJ, it was possible to obtain a maximum output density of $\approx 280nW$ ($-35dBm$) and a frequency $f_max\approx 0.85GHz$ obtained for a current $I=0.3mA$ (Figure \ref{fig:ultrab}). Through this method it was possible to obtain large oscillation power using ultra low current density of $J_c<5\times 10^5A/cm^2$ and in the absence of an external magnetic field. However, the phase noise of the oscillations created with this MTJ was very high (with a linewidth of $\Delta f\approx 30 MHZ$) making it unviable for RF oscillators.

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.6\linewidth]{img/ultralow.png}
  \caption{MTJ structure}
  \label{fig:ultraa}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.6\linewidth]{img/ultralow2.png}
  \caption{Output density for several current values}
  \label{fig:ultrab}
\end{subfigure}
\caption{}
\end{figure}

Grimaldi (et al.)(2013)\cite{vortex} used a vortex distribution of the magnetization of the free layer, (Figure \ref{vortex}) where there is an in-plane curling magnetization except in the center, where the magnetization points out-of-plane. The vortex distribution MTJ also allows for a zero external magnetic field functioning, as well as having fairly low phase noise (linewidth $\Delta f\approx 19 kHZ$). Unlike in the previously described experience, there is a need for a high current density for this MTJ to work, making it as well unviable for RF oscillations.   


\begin{figure}[H]
\centering
\resizebox{.3\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/vortex.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{Vortex free layer of a MTJ}
    \label{fig:vortex}
\end{figure}


Maehara (et al.)(2014)\cite{sombrero} proposes a new "sombrero shaped" nano-contact structure for the MTJ. Using common nano-pillar structures spin precession may become spatially incoherent mainly due to extrinsic disturbances such as inhomogeneous stray fields from the reference layer. Another structure commonly used is the nano-contact that have high $Q$ factors (meaning narrow linewidth), though they are incompatible with MTJ's because a major portion of the current would flow through the free layer and capping layer without passing through the tunnel barrier which significantly reduces both the MR ratio and spin transfer torque. Using a "sombrero-shaped" nano-contact we can have similar values of 'Q' factor without reducing the MR ratio or the spin transfer torque. With these new nano structures it was possible to obtain a maximum output density of $2.4 \mu W$ ($-26dBm$) with linewidth $\Delta f=12MHz$. It is also worth noticing that these values were obtained using a common MTJ, meaning that even better results might be obtained using the experimental MTJ designs mentioned earlier. In figure \ref{fig:ult} are shown the three nano structures mentioned.

\begin{figure}[H]
\centering
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{img/pillar.png}
  \caption{nano-pillar structure}
  \label{fig:1}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{img/contact.png}
  \caption{nano-contact structure}
  \label{fig:2}
\end{subfigure}
\begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{img/sombrero.png}
  \caption{"sombrero-shaped" nano-contact structure}
  \label{fig:3}
\end{subfigure}%
\caption{}
\label{fig:ult}
\end{figure}


\section{Fabrication}
The fabrication of these devices has usually two steps:
the magnetic stack deposition and the nanofabrication.

In order to obtain a high 
quality MTJ an accuracy of 10pm in the thickness of the layers is needed,
therefore the methods of fabrication used must be very precise.
The magnetic stack of a STNO is fabricated using thin film technology.
The films are deposited using physical vapor deposition. These 
processes vaporize material (usually by sputtering)
from a solid or liquid source material that are transported over 
a vacuum to a substrate where it condenses. With this process it is possible to obtain layers from a few nanometers to thousands of nanometers thick.
By depositing several layers on top of each other a magnetic stack
can be built.

After the deposition the layers are amorphous and need to be annealed 
in order to crystalize, so that a high TMR coefficient can be obtained.
The magnetic stack is usually a magnetic tunnel junction which is 
used for its high magnetoresistance.

State of the art MTJ are built using CoFeB/MgO/CoFeB trilayers 
which have a high magnetoresistance coefficient.
Typical values for the TMR coefficient of devices obtained with these
processes range from 40\% to 130\%.

\begin{figure}[H]
\centering
\resizebox{.7\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/sputtering.png}
  \unskip
   }
\captionsetup{type=figure}
    \caption{Sputtering process, where a material is vaporized by the means of
	     colisions with energetic atomic-scale bombarding particles}
    \label{img:fft}
\end{figure}

The nanofabrication process consists of standard microelectronics
fabrication processes, usually consisting of some levels of 
photolithography, dry and wet etching, metal and insulator deposition
and planarization. When building these devices on a small scale 
e-beam lithography and ion beam etching are used instead of photolithography
which is better suited to mass production but requires a set of photomasks
that can be very expensive for low volume production.

\begin{figure}[H]
\centering
\resizebox{.3\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/mtj_001.png}
  \unskip
   }
\captionsetup{type=figure}
	\caption{Schematic of a microfabricated MTJ with relevant dimensions} 
    \label{img:fft}
\end{figure}

The structures fabricated with these methods take the form of 
nanopillars with diameters around 100nm.

\section{STNO based communications devices}

Currently wireless radio systems are ubiquitous on the consumer market.
The current generation of mobile phones has multiple radios transcivers
for multiple communications standards.
The basic means of wireless communications involves the mixing of the signal
that is going to be transmitted (the baseband signal) with a carrier signal. 
This creates a series of sidebands around the carrier signal.
In order to be able to communicate, an oscilator must have the ability to generate 
the carrier signal which is usually in the order of GHz.

\begin{figure}[H]
\centering
\resizebox{.7\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/het.png}
  \unskip
   }
\captionsetup{type=figure}
	\caption{Architeture of a typical radio reciever} 
    \label{img:fft}
\end{figure}

The STNO described in the previous sections have the potential
to replace classical crystal based oscillators coupled to a PLL for embedded
radio communications such as Wi-Fi or Bluetooth.
These types of communications require the use of a voltage controlled oscillator
in order to generate the modulated signal for transmission.

STNO based oscillators have several advantages compared to LC-tank based oscillators:
they have higher tunability, which can be adapted to multiple communications 
standards, they are much smaller and  consume less power, as when using 
LC based oscillators for multiple standards, multiple LC-tanks are needed
because of the lower tunability of these oscillators.

However STNO based VCO suffer from lower power output and from lower spectral 
purity when compared to LC based VCO. The lower spectral purity hinders 
the modulation of signals, which requires a mixer.

To reach an output level suibtable for current telecommunications standards 
STNO require high impedance RF amplifiers. Typical STNO based oscilators 
have an output of about -35dBm which is 3 decades below what is needed 
($\approx$0dBm) to drive a usual RF block such as a mixer.

The amplifier must have an input impedance that matches the impedance of the 
STNO  which is in the range of several hundred $\Omega$.  
The amplifier must also have an output impedance of $50\Omega$ to preserve 
compatibility with existing standard RF equipment. 
Because of the high frequencies involved it is very difficult to design a 
single stage amplifier with a high enough gain for this application.
A possible solution requires the use of a multiple stage amplifier where each amplifier
is a simple differential pair based cascode amplifier as was done by \cite{intro}.

\begin{figure}[H]
\centering
\resizebox{.2\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/cascode.png}
  \unskip
   }
\captionsetup{type=figure}
	\caption{Schematic of a cascode differential pair} 
    \label{img:fft}
\end{figure}

Using the amplifier design described above its possible to boost the output power
of STNO to acceptable levels but the issue of spectral purity remains
which is very important in order to obtain high performance frequency 
synthesis.

A Phase locked loop based architeture may provide a solution for the low
spectral purity of STNO. The PLL shown below uses an external quartz 
resonator as an accurate reference which is can be multiplied by a 
variable amount with the PLL.  The PLL consists of a phase comparator
where its output feeds the control input of a STNO. The output of 
the STNO is amplified as described above, and then its divided by a
variable amount with a frequency divider. By controling how much the signal 
is divided the output frequency of the PLL can be controled.

Since this design features a crystal resonator as a reference it has the 
potential to have a much higher spectral purity. However it suffers from
the relative large size of a crystal resonator when compared to the 
remainder of the circuit which can be compeletly built on a 
single integrated circuit.

\begin{figure}[H]
\centering
\resizebox{.6\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/pll.png}
  \unskip
   }
\captionsetup{type=figure}
	\caption{Architeture of STNO based PLL oscilator, which uses } 
    \label{img:fft}
\end{figure}

\section{Communication using frequency shift keying}

In order to communicate wirelessly with these devices a different technique
that does not require RF mixing for frequency up/down converting is frequency 
shift keying. 

\begin{figure}[H]
\centering
\resizebox{.6\columnwidth}{!}{%
\renewenvironment{figure}[1][]{\ignorespaces}{\unskip}%
  \centering
  \includegraphics[width=\columnwidth]{img/fsk.png}
  \unskip
   }
\captionsetup{type=figure}
	\caption{Example of a FSK modulated signal} 
    \label{img:fft}
\end{figure}

Frequency shift keying transmits data by varying the frequency of a carrier signal.
This can be done with STNO by varying the bias current that flows onto the device.
If we are transmitting information in this way the main parameter that will affect 
the maximum speed of data transmission is the response time of the STNO to changes
in $I_{dc}$. For current devices these times are inferior to 25ns \cite{fsk}. 
The frequency transitions that are obtained when modulating the bias current on the MTJ
are also phase coherent which is convinient for the implementation of these devices.

However in a straight forward implentation of FSK as described above there is a
significant out of band spectral leakage that can cause interference when 
multiple devices are communicating in the same area. 
This is a huge disadvantage when using this type of modulation.
